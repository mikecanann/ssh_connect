#!/usr/bin/ruby
#===============================================================================
#
#         FILE: ssh.rb
#
#        USAGE: ./ssh.rb
#
#  DESCRIPTION:
#                on windows, compile to exe with this command:
#                  ocra --no-autoload ssh.rb settings.yaml
#
#      OPTIONS: ---
# REQUIREMENTS: 
#                on linux run: sudo yum install ruby-qt
#                on windows run: gem install qtbindings
#                                gem install ocra
#
#         BUGS:
#                the function: toggle_always_on_top is not working on linux
#        NOTES:
#                   mrc - clean up the yaml stuff, putting it in function calls
#
#       AUTHOR: Mike Canann (mrc), mikecanann@gmail.com
#      COMPANY: t
#      VERSION: 1.0
#      CREATED: 2012-04-11 9:31:18 AM
#     REVISION: ---
#===============================================================================


require 'rubygems'
require 'yaml'
require 'Qt'

require 'thread'



class QtApp < Qt::Widget

   slots 'on_comboBox_site_activated(QString)'
   slots 'on_comboBox_host_name_activated(QString)'
   slots 'on_comboBox_user_name_activated(QString)'
   slots 'on_pushButton_Connect_clicked()'


   def initialize(parent = nil)
      super(parent)

      @debug = 0

      # default linux setting, using the keys in the user's .ssh directory
      @identity_file = ""

      # use ssh on linux and putty on windows
      @islinux = RUBY_PLATFORM.downcase.include?("linux")


      @connect_cmd = '"c:\Program Files\PuTTY\putty.exe" '

      if(@islinux)
         @connect_cmd = "gnome-terminal --execute ssh "
      end


      @save_sub_process = Array.new


      setWindowTitle("SSH")

      init_ui(parent)

      # index of dropdowns
      @site_data = 0
      @host_name_data = 0
      @user_name_data = 0

      show()

   end

   def init_ui(parent)

      @idRole = Qt::UserRole


      @label_1 = Qt::Label.new("Site:", self)
      @label_2 = Qt::Label.new("Description - Host:", self)
      @label_3 = Qt::Label.new("User:", self)

      @comboBox_site = Qt::ComboBox.new(self)
      @comboBox_host_name = Qt::ComboBox.new(self)
      @comboBox_user_name = Qt::ComboBox.new(self)

      @pushButton_Connect = Qt::PushButton.new(self)
      @pushButton_Connect.objectName = "pushButton_Connect"
      @pushButton_Connect.text = "Connect"




      @config_file_yaml = "#{File.expand_path(File.dirname(__FILE__))}/settings.yaml"

      puts @config_file_yaml

      if File.exist? @config_file_yaml
         @yaml_data = YAML.load_file( @config_file_yaml )
      else
         puts "yaml file missing"
         exit
      end


      @yaml_data['sessions'].each_index do |i|
         @comboBox_site.addItem(tr("#{@yaml_data['sessions'][i][0]['site']}"), Qt::Variant.new(i))
      end

      if(false == @islinux)
         if(@yaml_data['win_ssh_key'].length > 1)
            @identity_file = " -i #{@yaml_data['win_ssh_key']} "
         end
      end

      on_comboBox_site_activated("#{@yaml_data['sessions'][0][0]['site']}")



      connect(@pushButton_Connect, SIGNAL("clicked()"), self, SLOT("on_pushButton_Connect_clicked()"))
      connect(@comboBox_site, SIGNAL("activated(QString)"), self, SLOT("on_comboBox_site_activated(QString)"))
      connect(@comboBox_host_name, SIGNAL("activated(QString)"), self, SLOT("on_comboBox_host_name_activated(QString)"))
      connect(@comboBox_user_name, SIGNAL("activated(QString)"), self, SLOT("on_comboBox_user_name_activated(QString)"))




      @vbox = Qt::VBoxLayout.new
      @vbox.addWidget(@label_1)
      @vbox.addWidget(@comboBox_site)
      @vbox.addWidget(@label_2)
      @vbox.addWidget(@comboBox_host_name)
      @vbox.addWidget(@label_3)
      @vbox.addWidget(@comboBox_user_name)
      @vbox.addWidget(@pushButton_Connect)

      setLayout(@vbox)

      if(1 == @debug)
         puts @yaml_data['alwaysontop']
      end
      if(1 == @yaml_data['alwaysontop'])
         toggle_always_on_top()
      end

      if(1 == @debug)
         puts @yaml_data['opacity']
      end
      setWindowOpacity(@yaml_data['opacity'])

   end

   def toggle_always_on_top
      wflags = windowFlags();
      wflags ^= (Qt::X11BypassWindowManagerHint | Qt::WindowStaysOnTopHint).to_i
      setWindowFlags( wflags );
   end

   def on_comboBox_site_activated text

      @site_data = @comboBox_site.itemData(@comboBox_site.currentIndex(), @idRole).toInt
      # remove all the servers for this site
      while(@comboBox_host_name.count > 0)
         @comboBox_host_name.removeItem(0)
      end

      @yaml_data['sessions'][@site_data][1].each_index do |i|
         @comboBox_host_name.addItem(
            tr("#{@yaml_data['sessions'][@site_data][1][i]['desc']} - #{@yaml_data['sessions'][@site_data][1][i]['host']}"),
            Qt::Variant.new(i))
      end

      on_comboBox_host_name_activated(@yaml_data['sessions'][@site_data][1][0]['host'])

   end # end on_comboBox_site_activated text

   def on_comboBox_host_name_activated text
      #@label_1.setText(text)
      #@label_1.adjustSize
      @host_name_data = @comboBox_host_name.itemData(
                           @comboBox_host_name.currentIndex(),
                           @idRole).toInt


      # remove all the users and add the users for this server
      while(@comboBox_user_name.count > 0)
         @comboBox_user_name.removeItem(0)
      end

      @yaml_data['sessions'][@site_data][1][@host_name_data]['users'].each_index do |i|
         @comboBox_user_name.addItem(
            tr(@yaml_data['sessions'][@site_data][1][@host_name_data]['users'][i][0]['user']),
            Qt::Variant.new(i))
      end

      on_comboBox_user_name_activated(@yaml_data['sessions'][@site_data][1][@host_name_data]['users'][0][0]['user'])
   end

   def on_comboBox_user_name_activated text
      @user_name_data = @comboBox_user_name.itemData(@comboBox_user_name.currentIndex(), @idRole).toInt


      ssh_cmd = "#{@connect_cmd} #{@identity_file} #{@yaml_data['sessions'][@site_data][1][@host_name_data]['users'][@user_name_data][0]['user']}@#{@yaml_data['sessions'][@site_data][1][@host_name_data]['host']} "

      # default to Red (production)
      color = "660000"

      # server description includes 'test' or 'Stage' then it is a test machine
      if(@yaml_data['sessions'][@site_data][0]['site'].include?("Test"))
         # display those as green
         color = "008000"
      end

      ssh_cmd = "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">
<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">
p, li { white-space: pre-wrap; }
</style></head><body style=\" \">
<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" color:##{color};\"> #{ssh_cmd} </span></p></body></html>"

   end # end on_comboBox_host_name_activated text

   def on_pushButton_Connect_clicked()

      puts "Connect to host"

      ssh_cmd = "#{@connect_cmd} #{@identity_file} #{@yaml_data['sessions'][@site_data][1][@host_name_data]['users'][@user_name_data][0]['user']}@#{@yaml_data['sessions'][@site_data][1][@host_name_data]['host']} "


      puts ssh_cmd


      @new_process = Qt::Process.new()
      @new_process.start(tr(ssh_cmd))


      # save the list so garbage collection doesn't kill
      #     the processes?  - if that is what is killing them
      # This did fix the problem of windows closing at random
      @save_sub_process << @new_process
   end # end on_pushButton_Connect_clicked()


end

app = Qt::Application.new ARGV
my_app = QtApp.new
my_app.show
app.exec
