ssh_connect
===========

Ruby/Qt menu to open SSH connections


Uses 'Putty.exe' on Windows, and 'ssh' on Linux. To define the servers and users, set them up in the settings.yaml. An example of settign up a server:
```YAML
- - site: Web Stage
 - - host: v01server1t
     desc: stage web server 1
     users:
       - [user: 'webuser']
       - [user: 'root']
       - [user: 'username']
```

That config would look like this in Linux:

![linux screenshot](/linux.png "Linux Screenshot")


And would look like this in Windows:

![win screenshot](/windows.png "Windows Screenshot")
